Simple PhoneGap 2.9.0 battery events demo.

Tested on four different devices and Android versions:

- Samsung Galaxy Nexus with Android 4.3 (previously Android 4.2.2),
- Sony Xperia E with Android 4.1.1,
- LG GT540 with Android 2.3.3 and CyanogenMod, 
- GSmart Rola G1317D with Android 2.2.2.

All seems to be fine, after implementing [this small fix](https://github.com/phonegap/build/issues/178#issuecomment-24287410).
